<?php
function tree($h) {
	$result = "";
	for ($i = 1; $i <= $h; $i++) {
		for ($j = $i; $j < $h; $j++) {
			$result .= " ";
		}

		for ($k = $h - $i; $k < $h; $k++) {
			$result .= "*";	
		}
		if ($i != $h) {
			$result = $result . "\n";
		}
	}

	return $result;
}

function test($description, $actual, $expected) {
	echo $description . "\n";

	$result = 'passed';
	if ($actual !== $expected) {
		$result = 'failed';
	}

	echo "Test result: ${result}\n";
}
test("Test Pattern minus one: ", tree(-1), "");
test("Test Pattern zero: ", tree(0), "");
test("Test Pattern one: ", tree(1), "*");
test("Test Pattern two: ", tree(2), " *\n**");
test("Test Pattern Five: ", tree(5), "    *\n   **\n  ***\n ****\n*****");
?>
