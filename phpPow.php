<?php
function phpPow($x, $n) {
	if (!is_int($x) || !is_int($n)) {
		$output = false;
	}
	$output = 1;
	if ($n > 0) {
			for ($i = 1; $i <= $n; $i++) {
					$output = $output * $x;
			}			
	} else if ($n < 0) {
		  for ($i = -1; $i >= $n; $i--) {
		  		$output = $output / $x;
		  } 
	}

	return $output;
}

function test($description, $actual, $expected) {
	echo $description . "\n";

	$result = 'passed';
	if ($actual !== $expected) {
		$result = 'failed';
	}

	echo "Test result: ${result}\n";
}

test("Test phpPow(2,3)", phpPow(2, 3), pow(2, 3));
test("Test phpPow(2,-3)", phpPow(2, -3), pow(2, -3));
test("Test phpPow(-2,3)", phpPow(-2, 3), pow(-2, 3));
test("Test phpPow(-2,-3)", phpPow(-2, -3), pow(-2, -3));
test("Test phpPow(2,0)", phpPow(2, 0), pow(2, 0));
test("Test phpPow(-2,0)", phpPow(-2, 0), pow(-2, 0));
?>
